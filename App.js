import React from 'react';
import RootApp from './Root';

export default class App extends React.Component {
  render() {
    return <RootApp />;
  }
}
