import React, {Component} from 'react';
import {View, Text, StyleSheet, Platform} from 'react-native';
import {
  RTCPeerConnection,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStream,
  MediaStreamTrack,
  mediaDevices,
  registerGlobals,
} from 'react-native-webrtc';
import {TouchableOpacity} from 'react-native-gesture-handler';

let showChartRoom;
let localStream = null;
let remoteStream = null;
let isFront = false;

const MESSAGE_TYPE = {
  SDP: 'SDP',
  CANDIDATE: 'CANDIDATE',
};

const onPressCall = () => {
  console.log('onPressCall');
  startCall();
};

const startCall = async () => {
  try {
    // const stream = mediaDevices.getUserMedia({audio: true, video: true});
    // showChartRoom = true;

    const signaling = new WebSocket('ws://echo.websocket.org');
    const peerConnection = createPeerConnection(signaling);

    addMessageHandler(signaling, peerConnection);

    // stream
    //   .getTracks()
    //   .forEach((track) => peerConnection.addTrack(track, stream));
    // localStream = stream.toURL();
    mediaDevices.enumerateDevices().then((sourceInfos) => {
      console.log('sourceInfos', sourceInfos);
      let videoSourceId;
      for (let i = 0; i < sourceInfos.length; i++) {
        const sourceInfo = sourceInfos[i];
        if (
          sourceInfo.kind === 'videoinput' &&
          sourceInfo.facing === (isFront ? 'front' : 'environment')
        ) {
          videoSourceId = sourceInfo.deviceId;
        }
      }
      mediaDevices
        .getUserMedia({
          audio: true,
          video: {
            mandatory: {
              minWidth: 500, // Provide your own width, height and frame rate here
              minHeight: 300,
              minFrameRate: 30,
            },
            facingMode: isFront ? 'user' : 'environment',
            optional: videoSourceId ? [{sourceId: videoSourceId}] : [],
          },
        })
        .then((stream) => {
          localStream = stream.toURL();
          peerConnection.addStream(stream);
        })
        .catch((error) => {
          console.log('error =>', error);
        });
    });
  } catch (error) {
    console.log('start call error =>', error);
  }
};

const createPeerConnection = (signaling) => {
  const peerConnection = new RTCPeerConnection({
    iceServers: [{urls: 'stun:stun.l.google.com:19302'}],
  });

  peerConnection.onnegotiationneeded = async () => {
    await createAndSendOffer(signaling, peerConnection);
  };

  peerConnection.onicecandidate = (iceEvents) => {
    if (iceEvents && iceEvents.candidate) {
      signaling.send(
        JSON.stringify({
          message_type: MESSAGE_TYPE.CANDIDATE,
          content: iceEvents.candidate,
        }),
      );
    }
  };

  peerConnection.ontrack = (event) => {
    remoteStream = event.streams[0];
  };

  return peerConnection;
};

const addMessageHandler = (signaling, peerConnection) => {
  signaling.onmessage = async (message) => {
    const data = JSON.parse(message.data);
    if (!data) {
      return;
    }

    const {message_type, content} = data;
    console.log('data =>', data);
    try {
      if (message_type === MESSAGE_TYPE.CANDIDATE && content) {
        await peerConnection.addIceCandidate(content);
      } else if (message_type === MESSAGE_TYPE.SDP) {
        if (content.type === 'offer') {
          await peerConnection.setRemoteDescription(content);
          const answer = await peerConnection.createAnswer();
          await peerConnection.setLocalDescription(answer);
          signaling.send(
            JSON.stringify({
              message_type: MESSAGE_TYPE.SDP,
              content: answer,
            }),
          );
        } else if (content.type === 'answer') {
          await peerConnection.setRemoteDescription(content);
        } else {
          console.log('Unsupported SDP type.');
        }
      }
    } catch (error) {
      console.log('addMessageHandler error =>', error);
    }
  };
};

const createAndSendOffer = async (signaling, peerConnection) => {
  const offer = await peerConnection.createOffer();
  await peerConnection.setLocalDescription(offer);

  signaling.send(
    JSON.stringify({message_type: MESSAGE_TYPE.SDP, content: offer}),
  );
};

class Main extends Component {
  render() {
    return (
      <View style={styles.container}>
        {showChartRoom ? (
          <View style={styles.styles.videoContainer}>
            <RTCView streamURL={localStream} />
            <RTCView streamURL={remoteStream} />
          </View>
        ) : (
          <TouchableOpacity style={styles.btnWrapper} onPress={onPressCall}>
            <Text style={styles.btnText}>Start video call</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  videoContainer: {
    flex: 1,
    justifyContent: 'space-between',
    borderColor: 'green',
    backgroundColor: 'black',
  },
  btnWrapper: {
    backgroundColor: '#0449',
    width: 150,
    height: 30,
    borderRadius: 7,
  },
  btnText: {
    color: 'white',
    textAlign: 'center',
    padding: 5,
  },
});

export default Main;
