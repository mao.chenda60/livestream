import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';
import {
  RTCPeerConnection, // to represent a connection between the local and remote
  RTCIceCandidate, // to check user's internet connectivity
  RTCSessionDescription,
  RTCView,
  MediaStream,
  MediaStreamTrack,
  mediaDevices,
  registerGlobals,
} from 'react-native-webrtc';

let isFront = false;
let localStream = null;
let remoteStream = null;
let connectedUser = null;
let socketActive = false;
let callToUsername = '';
let webSocketConnection;

const configuration = {
  iceServers: [{url: 'stun:stun.l.google.com:19302'}],
};
const peerConnection = new RTCPeerConnection(configuration);

// const webSocketConnection = new WebSocket('ws://echo.websocket.org'); // ws://10.0.2.2:1337

mediaDevices.enumerateDevices().then((sourceInfos) => {
  // console.log('sourceInfos', sourceInfos);
  let videoSourceId;
  for (let i = 0; i < sourceInfos.length; i++) {
    const sourceInfo = sourceInfos[i];
    if (
      sourceInfo.kind === 'videoinput' &&
      sourceInfo.facing === (isFront ? 'front' : 'environment')
    ) {
      videoSourceId = sourceInfo.deviceId;
    }
  }
  mediaDevices
    .getUserMedia({
      audio: true,
      video: {
        // mandatory: {
        //   minWidth: 500, // Provide your own width, height and frame rate here
        //   minHeight: 300,
        //   minFrameRate: 30,
        // },
        video: true,
        facingMode: isFront ? 'user' : 'environment',
        optional: videoSourceId ? [{sourceId: videoSourceId}] : [],
      },
    })
    .then((stream) => {
      localStream = stream.toURL();
      peerConnection.addStream(stream);
    })
    .catch((error) => {
      console.log('error =>', error);
    });
});

peerConnection.onaddstream = (event) => {
  console.log('onaddstream event =>', event);
  remoteStream = event.stream.toURL();
};

peerConnection.onicecandidate = (event) => {
  console.log('peerconnection.onicecandidate =>', event);
  if (event.candidate) {
    send({
      type: 'candidate',
      candidate: event.candidate,
    });
  }
};

webSocketConnection = new WebSocket('ws://localhost:1337');
// console.log('websocketconnect 1 =>', webSocketConnection);

webSocketConnection.onopen = () => {
  console.log('connected to the signaling server');
  socketActive = true;
};

webSocketConnection.onerror = (error) => {
  console.log('Error while connecting to socket =>', error);
};

webSocketConnection.onmessage = (msg) => {
  console.log('websocketconnection msg =>', msg);
  let data;
  if (msg.data === 'Hello world') {
    data = {};
  } else {
    data = JSON.parse(msg.data);
    // console.log('onmessage data =>', data);
    switch (data.type) {
      case 'login':
        console.log('Login');
        break;

      case 'offer':
        handleOffer(data.offer, data.name);
        break;

      case 'answer':
        handleAnswer(data.answer);
        break;

      case 'candidate':
        handleCandidate(data.candidate);
        break;

      case 'leave':
        handleLeave();
        break;

      default:
        break;
    }
  }
};

const send = (message) => {
  // console.log('send webSocketConnection =>', webSocketConnection);
  console.log('send message =>', message);
  // console.log('send connectedUser =>', connectedUser);
  if (connectedUser) {
    message.name = connectedUser;
    // console.log('connecteduser in end =>', message);
  }
  webSocketConnection.send(JSON.stringify(message));
};

const onPressCall = async () => {
  console.log('onPressCall peerConnection =>', peerConnection);

  connectedUser = callToUsername;

  try {
    const offer = await peerConnection.createOffer();
    // console.log('offer 1 =>', offer);
    // createoffer ot requesting connected to others
    await peerConnection.setLocalDescription(offer).then(() => {
      // console.log('sending offer 2');
      console.log('offer', offer);
      send({
        type: 'offer',
        offer: offer,
      });
    });
  } catch (error) {
    console.log('create offer error =>', error);
  }
};

const handleOffer = async (offer, name) => {
  console.log('handleOffer offer, name =>', offer, name);
  // console.log('handleOffer peerConnection =>', peerConnection);

  try {
    const configuration1 = {
      iceServers: [{url: 'stun:stun1.l.google.com:19302'}],
    };
    const remote_pc = new RTCPeerConnection(configuration1);
    await remote_pc.setRemoteDescription(new RTCSessionDescription(offer));
    const answer = await remote_pc.createAnswer();
    await remote_pc.setLocalDescription(answer);
    send({
      type: 'answer',
      answer: answer,
    });
  } catch (error) {
    console.log('handleOffer error =>', error);
  }
};

const handleLeave = () => {
  // console.log('handleLeave peerConnection => ', peerConnection);
  this.setState({
    remoteStream: null,
  });
  connectedUser = null;
  peerConnection.close();
};

const handleAnswer = (answer) => {
  console.log('handleanswer answer', answer);
  // console.log('handleAnswer peerConnection =>', peerConnection);
  peerConnection.setRemoteDescription(new RTCSessionDescription(answer));
};

const handleCandidate = (candidate) => {
  console.log('handleCandidate candidate =>', candidate);
  // console.log('handleCandidate peerConnection =>', peerConnection);
  peerConnection.addIceCandidate(new RTCIceCandidate(candidate));
};

class CallScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId1: '',
      calling: false,
      offer: null,
      // callToUsername: '',
    };
  }

  componentDidMount() {
    this.getUserId();
  }

  getUserId = () => {
    // console.log('getUserId()');
    const {navigation} = this.props;
    AsyncStorage.getItem('userId').then((id) => {
      // console.log('id', id);
      if (id) {
        this.setState({
          userId1: id,
        });
      } else {
        navigation.navigate('Login');
      }
    });
  };

  onChangeText = (text) => {
    callToUsername = text;
    // this.setState({
    //   callToUsername: text,
    // });
  };

  onPressLogout = () => {
    const {navigation} = this.props;
    AsyncStorage.removeItem('userId').then(() => {
      navigation.push('Login');
    });
  };

  render() {
    const {userId1} = this.state;
    console.log('localStream =>', localStream);
    console.log('remoteStream =>', remoteStream);
    console.log('socketActive =>', socketActive);
    return (
      <View style={styles.container}>
        <View style={styles.inputFields}>
          <Text style={styles.username}>{`Username: ${userId1}`}</Text>
          <TouchableOpacity onPress={() => this.onPressLogout()}>
            <Text style={styles.username}>logout</Text>
          </TouchableOpacity>
          <TextInput
            style={styles.input}
            placeholder="user 2"
            onChangeText={(text) => this.onChangeText(text)}
          />
          <TouchableOpacity style={styles.btnCall} onPress={onPressCall}>
            <Text style={styles.btnContent}>Call</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.videoContainer}>
          <View style={[styles.videos, styles.localVideos]}>
            <Text>Your Video</Text>
            <RTCView streamURL={localStream} style={styles.localVideo} />
          </View>
          <View style={[styles.videos, styles.remoteVideos]}>
            <Text>Friends Video</Text>
            <RTCView streamURL={remoteStream} style={styles.remoteVideo} />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20,
  },
  username: {
    fontSize: 16,
    fontWeight: '500',
    color: 'black',
    textAlign: 'right',
    padding: 15,
  },
  input: {
    height: 40,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: 'gray',
  },
  inputFields: {
    marginBottom: 10,
    flexDirection: 'column',
  },
  btnCall: {
    height: 40,
    alignItems: 'stretch',
    justifyContent: 'center',
    fontSize: 18,
    backgroundColor: '#004390',
    marginVertical: 10,
  },
  btnContent: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 18,
    fontWeight: '600',
  },
  videoContainer: {
    flex: 1,
    minHeight: 450,
  },
  videos: {
    width: '100%',
    flex: 1,
    position: 'relative',
    overflow: 'hidden',

    borderRadius: 6,
  },
  localVideos: {
    height: 100,
    marginBottom: 10,
  },
  remoteVideos: {
    height: 400,
  },
  localVideo: {
    backgroundColor: '#f2f2f2',
    height: '100%',
    width: '100%',
  },
  remoteVideo: {
    backgroundColor: '#f2f2f2',
    height: '100%',
    width: '100%',
  },
});

export default CallScreen;
