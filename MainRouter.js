import React, {Component} from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';

import Login from './Login';
import CallScreen from './CallScreen';

const MainRouter = createStackNavigator(
  {
    Login: {screen: Login},
    CallScreen: {screen: CallScreen},
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none',
  },
);

const App = createAppContainer(MainRouter);

export default App;
