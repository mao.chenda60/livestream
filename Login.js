import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

class Login extends Component {
  state = {
    isLoading: false,
    userId: '',
  };

  componentDidMount() {}

  onChangeText = (text) => {
    // console.log('text', text);
    this.setState({
      userId: text,
    });
  };

  onLogin = () => {
    const {userId} = this.state;
    const {navigation} = this.props;
    // console.log('navigation', this.props);

    try {
      this.setState({isLoading: true});
      setTimeout(() => {
        AsyncStorage.setItem('userId', userId);
        navigation.navigate('CallScreen', {userId: userId});
      }, 1000);
    } catch (error) {
      console.log('error =>', error);
    }

    this.setState(
      {
        isLoading: true,
      },
      () => {
        setTimeout(() => {
          navigation.navigate('CallScreen', {userId: userId});
        }, 1000);
      },
    );
  };

  render() {
    const {isLoading} = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.textTitle}>Enter your id</Text>
          <TextInput
            label="user1 id"
            onChangeText={(text) => this.onChangeText(text)}
            style={styles.input}
          />
          <TouchableOpacity onPress={() => this.onLogin()} style={styles.btn}>
            {!isLoading ? (
              <Text style={styles.btnContent}>Login</Text>
            ) : (
              <ActivityIndicator size="small" color="white" />
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  content: {
    paddingHorizontal: 20,
    justifyContent: 'center',
  },
  textTitle: {
    fontSize: 18,
    marginBottom: 10,
    fontWeight: '500',
  },
  input: {
    height: 60,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: 'gray',
  },
  btn: {
    height: 60,
    alignItems: 'stretch',
    justifyContent: 'center',
    fontSize: 18,
    backgroundColor: '#004390',
  },
  btnContent: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 18,
    fontWeight: '600',
  },
});

export default Login;
